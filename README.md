# můj nový projekt

Tady vzniká můj nový projekt.

# co je potřeba ještě udělat ?

- založit nový projekt

# zdroje

## git

- video kurz od Yablka [Git za hodinu a pol](https://www.youtube.com/playlist?list=PLhB6F20C-jTNSqo4VJT1QFVNqTP9DrPA8)

## nový repozitář na vps

Takto se vytvoří projekt pruga na vps, který je dostupný přes doménu domogled.com.

```shell
ssh pruga@domogled.com
# mkdir -p /barbaros/git/domogled
cd /barbaros/git/domogled
git init --bare pruga.git
```

